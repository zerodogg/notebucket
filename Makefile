SHELL=/bin/bash
MINIFY=$(shell if which gominify &>/dev/null; then echo "gominify";else echo "minify";fi)
default: build
distrib: build robots compress
robots:
	(echo ""; wget -O- 'https://raw.githubusercontent.com/ai-robots-txt/ai.robots.txt/refs/heads/main/robots.txt') >> public/robots.txt
build:
	rm -rf public/
	perl src/build
compress:
	for ftype in html css js; do\
		for file in $$(find public \( -name "*.$$ftype" \)); do \
			cat "$$file"|$(MINIFY) --html-keep-document-tags --type "$$ftype" -o "$$file";\
		done; \
	done
	find public \( -name '*.html' -o -name '*.ics' -o -name '*.json' -o -name '*.css' -o -name '*.js' \) -print0 | xargs -0 gzip -9 -k
	find public \( -name '*.html' -o -name '*.ics' -o -name '*.json' -o -name '*.css' -o -name '*.js' \) -print0 | xargs -0 brotli -k
clean:
	rm -f *~ */*~
devserver: build
	cd public && python -m http.server 8000
